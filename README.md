# Ansible Role GCloud

Role for installing gcloud.

## Requirements
This role requires Ansible 1.4 or higher, and platform requirements are listed in the metadata file.

## Installation

add this in **requirements.yml**

```shell
- src: https://gitlab.com/araulet-team/devops/ansible/ansible-role-gcloud
  name: araulet.gcloud
```

## Role Variables
The variables that can be passed to this role and a brief description about them are as follows:

```yaml
roles:
  - role: araulet.gcloud
    become: true
    vars:
      gcloud_install: true
```
## Dependencies

None

## Licence
MIT

## Author Information
Arnaud Raulet